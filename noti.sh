#!/bin/bash
set -euo pipefail

FAILURE=1
SUCCESS=0

MSTEAMSWEBHOOKURL=https://zylwin01outlook.webhook.office.com/webhookb2/74fb0b08-1e7a-4544-b7ca-796fe73b55ba@9b051b61-b0e1-40b2-96cb-15b7b8ec77ae/IncomingWebhook/844c8d4b7a6e47e9b97e8c33ebd1df5a/cee615b3-fb47-4cdf-8303-43d0b6ddeaa8

if [[ "${EXIT_STATUS}" == "${SUCCESS}" ]]; then
    jobStatus="succeeded"
    themeColor="00A300"

else
    jobStatus="failed"
    themeColor="FF2E2E"
fi

generate_summary_data() {
    cat <<EOF
{
    "@type": "MessageCard",
    "@context": "http://schema.org/extensions",
    "themeColor": "${themeColor}",
    "summary": "Gitlab pipeline ${jobStatus}.",
    "sections": [
        {
            "activityTitle": "${CI_JOB_NAME} job ${jobStatus}.",
            "activitySubtitle": "${CI_PROJECT_NAME}",
            "activityImage": "https://styles.redditmedia.com/t5_2zjmj/styles/communityIcon_prhqz913vkx81.png",
            "facts": [
                {
                    "name": "Pipeline ID",
                    "value": "${CI_PIPELINE_ID}"
                },
                {
                    "name": "Job Name",
                    "value": "${CI_JOB_NAME}"
                },
                {
                    "name": "Commit Branch",
                    "value": "${CI_COMMIT_REF_NAME}"
                },
                {
                    "name": "Status",
                    "value": "${jobStatus}"
                }
            ],
            "markdown": true
        }
    ],
    "potentialAction": [
        {
            "@type": "OpenUri",
            "name": "Go to Job",
            "targets": [
                {
                    "os": "default",
                    "uri": "${CI_JOB_URL}"
                }
            ]
        }
    ]
}
EOF
}


function send_pipeline_update() {

    local teams_webhook
    teams_webhook="$MSTEAMSWEBHOOKURL"
    curl -H 'Content-Type: application/json' -d "$(generate_summary_data)" "${teams_webhook}"
    echo "finish send_pipeline_update"
}

result=$(send_pipeline_update)
